package com.example.pacio.msk_09;

import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private int height=300;
    private  int width=300;
    RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rl=findViewById(R.id.rel);


        rl.setOnTouchListener(new View.OnTouchListener() {
            ImageView zwracane;
            HashMap<String , ImageView> mapa=new HashMap<String, ImageView>();

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                int x= (int)motionEvent.getX();
                int y= (int)motionEvent.getY();
                //int id=motionEvent.getPointerId(motionEvent.getPointerCount());

                switch(motionEvent.getActionMasked()){
                    case MotionEvent.ACTION_DOWN:{
                        Log.d("klik","kliko");
                      //  mapa.put(id,createImageView(x,y));
                        zwracane=createImageView(x,y);
                        break;
                    }
                    case MotionEvent.ACTION_UP: {

                        rl.removeView(zwracane);

                        break;
                    }
                    case MotionEvent.ACTION_MOVE:{
                        moveShape(x,y,zwracane);

                        break;
                    }
                }


                return true;
            }
        });


    }
    private void moveShape(int x, int y, ImageView iv){
        RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) iv.getLayoutParams();
        params.topMargin=(y - height/2);
        params.leftMargin=(x - width/2);
        iv.setLayoutParams(params);
    }

    private ImageView createImageView(int x, int y){
        ImageView iv=new ImageView(this);
        RelativeLayout.LayoutParams rlP=new RelativeLayout.LayoutParams(height, width);
        int tmp=width/2;
        rlP.leftMargin=(x-tmp);
        rlP.topMargin=(y-tmp);
        iv.setLayoutParams(rlP);
        iv.setVisibility(View.VISIBLE);
        iv.setImageDrawable(createShape());
        rl.addView(iv);

        return iv;
    }

    private ShapeDrawable createShape(){
        ShapeDrawable redS=new ShapeDrawable(new OvalShape());
        redS.getPaint().setColor(Color.RED);
        redS.setIntrinsicHeight(height);
        redS.setIntrinsicWidth(width);
        return redS;
    }


}
